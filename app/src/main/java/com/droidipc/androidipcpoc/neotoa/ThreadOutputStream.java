package com.droidipc.androidipcpoc.neotoa;

import java.io.*;

public class ThreadOutputStream extends ServletOutputStream
{ private static final byte[] crlf = { 13, 10 };
  private PrintStream psout;
  private SocketHandler conn;
  private boolean headersWritten = false;
  private boolean allowBody = true;
//  private String charset = "ISO-8859-1";

  public ThreadOutputStream( OutputStream out, SocketHandler conn )
	{ psout = new PrintStream( out );
	  this.conn = conn;
	}

  // before the first byte is written to the OutputStream
  // all headers have to be output
  //  afterwards writeHeaders() does nothing
  //
  // this is the point where "Content-type:" guessing could
  // be implemented by checking if "Content-type:" headers
  // is missing and if so buffering the first 6 bytes for inspection
  // then associate buf.toLowerCase() <html> to text/html
  //                                  any other < to text/xml
  //                                  anything else to text/plain or no header
  private boolean writeHeaders() throws IOException
  { if( headersWritten )
      return allowBody;
    ByteArrayOutputStream baos = new ByteArrayOutputStream(2000);
    PrintStream baps = new PrintStream(baos);
    int resCode = conn.getStatus();
    int cl = 0;
    String h;
	  if( (h=conn.getHeaderStr()) != null )
    { do
      { if( h.startsWith("Content-length: ") )
        { if( resCode != SocketHandler.SC_NOT_MODIFIED )
            cl = h.charAt(16) - '0';
          else
            h = null;
        }
/*
        else if( h.startsWith("Content-type: ") )
        { int i = h.indexOf("charset=");
          if( i > 0 )
          { charset = h.substring(i+8);
          }
        }
*/
		if( h != null )
        { baps.print( h );
          baps.write(crlf);
        }
      }
      while( (h=conn.getHeaderStr()) != null );
      baps.write(crlf);
      baps.flush();
      baps.close();
      baos.flush();
      psout.write(baos.toByteArray(),0,baos.size());
      baos.close();
      headersWritten = true;
    }
    return allowBody=resCode!=SocketHandler.SC_NOT_MODIFIED && (cl>0 || (resCode!=SocketHandler.SC_MOVED_TEMPORARILY && resCode!=SocketHandler.SC_SEE_OTHER)); // had additionally: && resCode!=SocketHandler.SC_MOVED_PERMANENTLY));
  }

  public void write( int b ) throws IOException
  { if( writeHeaders() )
      psout.write( b );
  }

  public void write( byte[] b, int off, int len ) throws IOException
  { if( writeHeaders() )
      psout.write( b, off, len );
  }

  public void flush() throws IOException
  { writeHeaders();
    psout.flush();
  }

  public void close() throws IOException
  { writeHeaders();
    psout.flush();
    psout.close();
    headersWritten = false;
  }

  public void print( String s ) throws IOException
  { if( writeHeaders() )
      psout.print( s );
  }

  public void print( int i ) throws IOException
  { if( writeHeaders() )
      psout.print( i );
  }

  public void print( long l ) throws IOException
  { if( writeHeaders() )
        psout.print( l );
  }

  public void println( String s ) throws IOException
  { if( writeHeaders() )
      psout.println( s );
	}

  public void println( int i ) throws IOException
  { if( writeHeaders() )
      psout.println( i );
  }

  public void println( long l ) throws IOException
  { if( writeHeaders() )
      psout.println( l );
  }

  public void println() throws IOException
  { if( writeHeaders() )
      psout.println();
  }

}

