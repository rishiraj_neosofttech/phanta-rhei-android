package com.droidipc.androidipcpoc.neotoa;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.net.*;
import java.text.SimpleDateFormat;

public class HttpDispatcher {
	private static HttpDispatcher serve = null;

	public static final SimpleDateFormat gmtFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz",
			Locale.US);

	private static final String serverinfO = "Neotoa v1.1";
	private static String mainPort = "8090";

	private static String ip = "127.0.0.1";

	protected boolean down = false;

	private int socketCount = 0;
	/// Main routine
	public static void main(String[] args) { // Parse args.

	
		int port = 8090;
		int argc = args.length;
		int argn;
		for (argn = 0; argn < argc && args[argn].charAt(0) == '-'; ++argn) {
			if (args[argn].equals("-p") && argn + 1 < argc) {
				++argn;
				port = Integer.parseInt(mainPort = args[argn]);
			}
		}
		gmtFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

		// Create the server.
		HttpDispatcher serve = new HttpDispatcher(port, System.err);
//   	serve.setInitParameter("ip-addr",ip);

		// And run.
		System.out.println("Server started.");
		serve.serve();

		// And stop
		System.out.println("Server stopped.");
		System.exit(0);
	}

	public static HttpDispatcher getInstance() {
		return serve;
	}

	private int port;
	private static PrintStream logStream;

	/// Constructor.
	public HttpDispatcher(int port, PrintStream logStream) {
		this.port = port;
		HttpDispatcher.logStream = logStream;
	}

	protected Hashtable targets = new Hashtable(); // URL path is key

	/// Run the server. Returns only on errors.
	public void serve() { // initialize the targets hash
							// this needs to replaced by a procedure that loads
							// the mappoings from the APK file
		targets.put("/imei", "any object needed for invocation of IPC");

		try {
			Method hookMethod = Runtime.class.getMethod("addShutdownHook", new Class[] { Thread.class });
			hookMethod.invoke(Runtime.getRuntime(), new Object[] { createShutdownHook() });
			System.out.println("Shutdown hook added");
		} catch (NoSuchMethodException nsme) { // Pre-1.3 VM
			System.out.println("Shutdown hook not available in pre-1.3 VM");
		} catch (SecurityException se) { // Either a screwy setup, or permission deliberately denied
			System.err.println("Permission to add Shutdown hook denied");
		} catch (IllegalAccessException iae) { // The VM is FUBAR.
			System.err.println("Access to addShutdownHook() denied");
		} catch (InvocationTargetException ite) { // Runtime.addShutdownHook() failed
			Throwable te = ite.getTargetException();
			System.err.println("addShutdownHook() failed: " + te.getClass().getName() + ": " + te.getMessage());
		}

		// Start Session Timer
//    new SessionTimer( this, sessions, sess_ips ).start();

		// make MIME type hashtable
//    makeMime();

		System.out.println("creating socket");
		ServerSocket httpServerSocket = null;
		try {
			httpServerSocket = new ServerSocket(port, 100);
			httpServerSocket.setSoTimeout(1000);
		} catch (IOException e) {
			return;
		}

		int sockCount = -1;

		try {
			while (!down) {
				Socket socket = null;
				try {
					socket = httpServerSocket.accept();
					socket.setSoTimeout(60000);
					socket.setSoLinger(true, 60);
					socket.setKeepAlive(true);

					SocketHandler sh = new SocketHandler(socket, this);
					System.out.println("HTTP SocketHandler returned to HttpDispatcher.");
				} catch (InterruptedIOException e) {
					if (socket != null) {
						socket.close();
						socket = null;
					}
				}
			}
		} catch (IOException e) {
		} finally {
			try {
				httpServerSocket.close();
			} catch (IOException e) {
			}
		}
		socketCount = -1;
//    destroyAllServlets();
	}

	private Runnable createShutdownHook() {
		return new Thread() {
			public void run() {
				down = true;
				while (socketCount != -1)
					Thread.yield();
			}
		};
	}

	// Methods from ServletContext.

	/*
	 * /// Destroys all currently-loaded servlets. private void destroyAllServlets()
	 * { Enumeration en = servlets.elements();
	 * System.out.println("destroying all servlets"); while( en.hasMoreElements() )
	 * { Servlet servlet = (Servlet) en.nextElement(); servlet.destroy(); }
	 * servlets.clear(); }
	 */
	/// Returns the name and version of the web server under which the servlet
	// is running.
	// Same as the CGI variable SERVER_SOFTWARE.
	public String getServerInfo() {
		return serverinfO;
	}

}
