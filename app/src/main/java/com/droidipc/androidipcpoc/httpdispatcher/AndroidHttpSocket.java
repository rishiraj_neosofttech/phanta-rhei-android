package com.droidipc.androidipcpoc.httpdispatcher;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.droidipc.androidipcpoc.IpcClientService;
import com.droidipc.androidipcpoc.R;
import com.droidipc.serviceProvider.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Yashesh Bhatt on 15/09/18.
 */
public class AndroidHttpSocket extends Service implements ServiceConnection {


    private ServerSocket serverSocket;
    private static final String ACTION_STOP_SELF = "stopserver";
    private static final int NOTIFCATION_ID = 2018;

    private Messenger ipcClientMessenger = null;

    private Messenger imeiMessenger = new Messenger(new Handler() {

        @Override
        public void handleMessage(Message msg) {
            resultImei = msg.obj + "";
            isImeiRequested = false;
        }
    });

    boolean isImeiRequested = false;
    String resultImei = null;


    private void initServer() throws IOException {
        InetAddress addr = InetAddress.getByName(Utils.getIPAddress(true));
        serverSocket = new ServerSocket(9000,50,addr);
        //serverSocket.setSoTimeout(1000);
        Log.d("IPC SERVER AT",serverSocket.getInetAddress().getHostName());
        while (!serverSocket.isClosed()) {
            Socket incomingRequestSocket = serverSocket.accept();
            incomingRequestSocket.getOutputStream().write("Hi".getBytes("UTF-8"));
            incomingRequestSocket.shutdownOutput();
            incomingRequestSocket.shutdownInput();
            incomingRequestSocket.close();
            //incomingRequestSocket.setKeepAlive(true);
           // incomingRequestSocket.setSoTimeout( 60000 );
           // incomingRequestSocket.setSoLinger(true, 60);
          //  incomingRequestSocket.setKeepAlive(true);
            /*OutputStream os = incomingRequestSocket.getOutputStream();

            Message requestMessage = new Message();
            requestMessage.replyTo = imeiMessenger;
            requestMessage.what = IpcClientService.ACTION_GET_IMEI;
            try {
                ipcClientMessenger.send(requestMessage);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            isImeiRequested = true;
            while (isImeiRequested) {
                //wait till response.
            }
            os.write(resultImei.getBytes());
            os.close();
            incomingRequestSocket.close();
           // processRequest(incomingRequestSocket);*/
            //TODO: manage async flow.
            // new Thread(() -> processRequest(incomingRequestSocket)).start();
        }

    }

    private void processRequest(Socket socket) {

            Log.d("process incoming","start");


            Log.d("BUFFER incoming","start");






      //  while (true) {

                String l = null;
                try {
                    BufferedReader reader =
                     new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    Log.d("Buffer init","init");
                    l = reader.readLine();
                    Log.d("READ FROM SOCKET",l+"");
                } catch (IOException e) {
                  Log.d("IOEXP",e+"");
                }
                if(l!=null) {

                    Log.d("line", " " + l);
                    String requestString = l;
                    //TODO: Parse JSON form string
                    Log.d("REQ incoming"," "+requestString);
                    Message requestMessage = new Message();
                    switch (requestString
                            .toLowerCase()) {

                        case "calllog":

                            break;
                        case "imei":
                            requestMessage.replyTo = imeiMessenger;
                            requestMessage.what = IpcClientService.ACTION_GET_IMEI;
                            break;

                    }
                    isImeiRequested = true;
                    try {
                        ipcClientMessenger.send(requestMessage);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    while (isImeiRequested) {
                        //wait till response.
                    }
                    OutputStream os = null;
                    try {
                        os = socket.getOutputStream();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        os.write(resultImei.getBytes());
                      //  os.close();
                        socket.shutdownOutput();
                        socket.close();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else{
                    Log.d("READ NULL LINE","serversocket");
                }
               // }



        Log.d("THREAD END","socket");

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (ACTION_STOP_SELF.equals(intent.getAction())) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(NOTIFCATION_ID);
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            unbindService(this);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }
        startForeground(NOTIFCATION_ID, buildForegroundNotification());
        new Thread(() -> {
            try {
                startIpcClient();
                initServer();
                } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }


    private Notification buildForegroundNotification() {
        Intent stopSelf = new Intent(this, AndroidHttpSocket.class);
        stopSelf.setAction(ACTION_STOP_SELF);


        // Make notification show big text.
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("HTTP server foreground service.");
        bigTextStyle.bigText("Android foreground service is a android service which can run in foreground always, it can be controlled by user via notification.");
        // Set big text style.
        return new NotificationCompat.Builder(this, "channel-01")
                .addAction(android.R.drawable.ic_notification_clear_all, "Close Server", PendingIntent.getService(this, 0, stopSelf, PendingIntent.FLAG_CANCEL_CURRENT))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setStyle(bigTextStyle)
                .build();

    }

    private void startIpcClient() {
        Intent imeiIntent = new Intent(this, IpcClientService.class);
        bindService(imeiIntent, this, BIND_AUTO_CREATE);
    }


    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ipcClientMessenger = new Messenger(iBinder);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }
}
