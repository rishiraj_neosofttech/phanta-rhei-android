package com.droidipc.androidipcpoc;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.droidipc.androidipcpoc.httpdispatcher.AndroidHttpSocket;
import com.droidipc.androidipcpoc.neotoa.HttpDispatcher;
import com.droidipc.serviceProvider.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class DashboardActivity extends AppCompatActivity {

    Messenger messenger;
    private Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_dashboard);


        Messenger imeiResponseHandler = new Messenger(new Handler(){

            @Override
            public void handleMessage(Message msg) {
                runOnUiThread(()-> ((TextView)findViewById(R.id.txtResponse)).setText("IMEI: "+msg.obj.toString()));

            }
        });

       // Intent imeiIntent = new Intent(this,IpcClientService.class);
       /* ServiceConnection connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
               messenger = new Messenger(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };*/
       // bindService(imeiIntent,connection, Context.BIND_AUTO_CREATE);





      /*  findViewById(R.id.buttonImei).setOnClickListener((view)->{
           if(messenger!=null){
            try {
                Message msg = new Message();
                msg.replyTo = imeiResponseHandler;
                msg.what = IpcClientService.ACTION_GET_IMEI;
                messenger.send(msg);
                Log.d("CALLED","REMOTE");
            } catch (RemoteException e) {
                e.printStackTrace();
                Log.w("CLIENT REMOTE",e+"");
            }
            Log.d("CALLED","BINDSERVICE");
        }});*/

        startService(new Intent(this, AndroidHttpSocket.class));
        new Handler().postDelayed(()->{
            new Thread(()->{
                socket = new Socket();
                try {
                    Log.d("CONNECTING TO",InetAddress.getByName(Utils.getIPAddress(true)).toString());
                    socket.connect(new InetSocketAddress(
                            InetAddress.getByName(Utils.getIPAddress(true)),9000));
                    String[] st = new String[1];
                    st[0] = "-p";
                    HttpDispatcher.main(st);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        },500);




        findViewById(R.id.buttonImei).setOnClickListener((v)-> new Thread(()->{
            try {

                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                OutputStream outputStream  = socket.getOutputStream();

                outputStream.write("imei\n".getBytes());
                Log.d("BYTES TO SERVER","write");
                String imei;
                while(true) {
                     imei = input.readLine();
                    if(imei!=null){
                        break;
                    }
                }
                Log.d("IMEI ON SOCKET::",imei+"");
                String finalImei = imei;
                runOnUiThread(()-> ((TextView)findViewById(R.id.txtResponse)).setText("IMEI: "+ finalImei));

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("ERROR ON SOCKET::",e+"");
            }

        }).start());



    }

}
