/*
    HttpInputStream is an extension to InputStream that requires a
    YieldingInputStream as constructor parameter!
    HttpInputStream works in one of three modes:
      - unlimited reads
      - length limited reads
      - chunked reads.
    The current mode can be set/altered by calling setTotalExpected(param).
    A param>=0 supplies HttpInputStream with the limit for "length limited reads".
    If param is -1 the mode is "unlimit read" whereas -2 indicates "chunked
    reads". Note that the switch to "chunked reads" is only allowed at the
    moment when the next read of the underlying stream will return the first
    byte a the chunk length line.
    It introduces the concept of EndOfStream (Eos). In "unlimited reads" Eos is
    reached (and internally set) when the underlying InputStream has reached the
    end. Eos is reset when setTotalExpected() is called. It is an error to reset
    Eos when it was set in "unlimited reads" mode!
    In "length limited reads" mode Eos is set internally when the number of bytes
    have been read. In this case the HttpInputStream can be reused afterwards
    by calling setTotalExpected again (in any of the three modes).
    In "chunked reads" mode all reads are internally limited to the chunk sizes.
    When the end of a chunk has been reached the next chunk is automatically
    initialized, i.e. the next chunk size is read internally and set as the new
    limit for the next reads. When the last chunk is read (chunk size is 0)
    HttpInputStream skippes over all footers that may exist (discarding them).
    Finally Eos is set and HttpInputStream is ready to be reused.
    Eos can also be set externally using setEos().
    HttpInputStream also provides an isClosed() method to query if the stream
    has been closed. Once the HttpInputStream has been closed it cannot be used
    anymore.

*/
package us.breadex.neotoa;

import java.io.*;

public class HttpInputStream extends InputStream
{ 
  private static final int max_chunk = 0x400000;
  private static final int max_post = 0x1000000;
  private YieldingInputStream yin;
  private boolean closed = false;
  private int totalExpected = -1;
  private int totalRead = 0;
  private boolean eos = false;

  private boolean chunked = false;
  private int chunk_len = -1;
  private int post_len = 0;
  private byte[] footers = null;

  private boolean ssl;
  private boolean sslDebug = false;

  public HttpInputStream( InputStream in, SocketHandler conn )
  { yin = (YieldingInputStream)in;
    ssl = yin.isSslStream();
  }

  public HttpInputStream( InputStream in, SocketHandler conn, String name )
  { yin = (YieldingInputStream)in;
    ssl = yin.isSslStream();
  }
  
  
  // totalExpected: >=0   explicit input length
  //                ==-1  till underlying stream closes
  //                ==-2  chunked input stream
  public void setTotalExpected( int totalExpected )
  { if( !closed )
    { totalRead = 0;
      chunked = totalExpected == -2;
      if( chunked )
      { chunk_len = 0;
        footers = new byte[240];
      }
	    this.totalExpected = totalExpected;
      eos = false;
    }
  }
  
  // log to the default PrintStream
  public void logRequestCache()
  { yin.logRequestCache();
  }

  // log to 'logStream' as long as it is not null
  public void logRequestCache( PrintStream logStream )
  { yin.logRequestCache(logStream);
  }

  public void setEos()
  { eos = true;
  }


  // reads the start of a chunk.
  // sets eos in the process if appropriate
  private void startChunk() throws IOException
  { int c;
    int j = 6;
    chunk_len = 0;
    String s = "0123456789abcdef0123456789ABCDEF ";
    for( int i=0; i<j; i++ )
    { if( (c=yin.read()) == 0x0d )
      { // we ignore both one CR or one CR LF leading the chunk line !!!
        if( i > 0 )
          break;
        if( (c=yin.read()) == 0x0a )
        { if( j++ == 6 )
            continue;
        }
      }
      int d = s.indexOf(c);
      if( d < 0 )
      { throw new IOException( "malformed chunk" );
      }
      if( d < 32 )
      { chunk_len = chunk_len << 4 + ( d & 0x0f );
      }
    }
    if( (c=yin.read()) != 0x0a )
    { throw new IOException( "malformed chunk" );
    }
	  if( chunk_len >= max_chunk )
    { throw new IOException( "oversized chunk" );
    }
	  if( (post_len+=chunk_len) >= max_post )
    { throw new IOException( "oversized post" );
    }
	  if( chunk_len == 0 )
	  { // switch into unlimited mode and read footers
      chunked = false;
      totalExpected = -1;
      while( yin.readLine(footers,0,footers.length) > 2 )
        ;
      eos = true;
    }
  }

    // ServletInputStream.read(,,) that keeps track
    // of how many bytes have been read and stops reading when the
    // Content-Length limit has been reached.
    //
  public int read( byte[] b, int off, int len ) throws IOException
  { int done = 0;
    if( eos )
	    return -1;
    if( totalExpected == -1 )
      // unlimited read
      return yin.read(b,off,len);
    int toGo = chunked ? chunk_len : totalExpected - totalRead;
    // limit read to either chunk or 'totalExpected' (i.e. normally Content-length)
    if( (done=yin.read(b,off,Math.min(len,toGo))) < 0 )
      eos = true;
    else if( chunked && (chunk_len-=done) == 0 )
      startChunk();
    else if( (totalRead+=done) >= totalExpected )
      eos = true;
    return done;
  }

  // Starting at the specified offset, reads into the given array of
  // bytes until all requested bytes have been read or a '\n' is
  // encountered, in which case the '\n' is read into the array as well.
  public int readLine(byte b[], int off, int len) throws IOException
  { int done = 0;
    if( eos )
	    return -1;
	  if( totalExpected == -1 )
    { // unlimited read
      if( (done=yin.readLine(b,off,len)) > 0 )
        totalRead += done;
      return done;
    }
    int toGo = chunked ? chunk_len : totalExpected - totalRead;
    // limit read to either chunk or 'totalExpected' (i.e. normally Content-length)
    if( (done=yin.readLine(b,off,Math.min(len,toGo))) < 0 )
      eos = true;
    else if( chunked && (chunk_len-=done) == 0 )
      startChunk();
    else if( (totalRead+=done) >= totalExpected )
      eos = true;
    return done;
  }

    // ServletInputStream.read() that keeps track
    // of how many bytes have been read and stops reading when the
    // Content-Length limit has been reached.
    //
  public int read() throws IOException
  {
//System.out.println("HttpInputStream.read() "+totalExpected);
    if( eos )
	    return -1;
    int i = yin.read();
	  if( i == -1 )
	    eos = true;
	  else
	    ++totalRead;
	  return i;
  }

  // available() reports only chunk content as being available
  public int available() throws IOException
  { if( eos )
	    return 0;
    int cnt = yin.available();
    return chunked && cnt>chunk_len ? chunk_len : cnt ;
  }

  public void close() throws IOException
  { if( !closed )
      yin.close();
    eos = closed = true;
  }

  public boolean isClosed()
  { return closed;
  }

}
