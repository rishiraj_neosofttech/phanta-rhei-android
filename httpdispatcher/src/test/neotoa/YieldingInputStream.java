/*
    YieldingInputStream is an extension to FilterInputStream that never blocks
    when input isn't available but yields instead.
    It implements it's own support for mark and reset. Therefore subclasses
    can rely on mark support.
    The state of the internal mark buffer 'mbuf' is controlled by 'iPtr' pointing
    to the next input index and 'oPtr' indicating the number of input bytes so
    that 'iPtr'-'oPtr' points to the index from which the next output should occur.
    The whole content of the mark buffer has already at least once been reported
    via any of the read methods. The buffering occurs only to facilitate re-reads.
    The mark buffer is generated only when needed.
    YieldingInputStream has a second internal buffer that is always present, the
    line excess buffer 'lxbuf'. It is filled when the method readLine is called.
    'iLxb' points always to the next input index whereas 'oLxb' points to the
    next output index. Please note the different behaviour of 'oPtr' which grows
    together with 'iPtr' when the 'mbuf' is filled and 'oLxb' which is not altered
    when bytes are input into 'lxbuf'! None of the bytes from 'oLxb' to 'iLxb'
    have ever been reported to any of the read methods.
*/

package us.breadex.neotoa;

import java.io.*;
import java.util.Date;

public class YieldingInputStream extends FilterInputStream
{ private SocketHandler conn;

  private InputStream is;        // read(),read([]) block; read([],,) may read partially
  private String ip;
  private int lxlen = 4096;
  private byte[] lxbuf;                 // line excess buffer
  private int iLxb = 0;
  private int oLxb = 0;
  private byte[] mbuf = null;           // mark buffer
  private int iPtr = 0;
  private int oPtr = 0;
  private int limit = 0;
  private boolean unmarked = true;
  private boolean reread = false;

  private boolean byteCount = false;
  private int totalRead = 0;			// # of bytes read from socket (already reported plus still buffered)
  private int lastTotal = 0;
  private boolean byteCopy = false;
  private boolean signalMark = true;
  private boolean includeTime = false;
  private StringBuffer yieldInfo = null;
  private ByteArrayOutputStream baos = null;
  private PrintStream defaultLogStream = System.err;

  private boolean ssl;
  private boolean sslDebug = false;
  
  public YieldingInputStream( InputStream in, SocketHandler conn, String ipaddr )
  { super( in );
    is = this.in;
    this.conn = conn;
    ip = ipaddr;
    lxbuf = new byte[lxlen];
    ssl = is.toString().indexOf(".ssl.") > 0;
  }

  public YieldingInputStream( InputStream in, SocketHandler conn, String ipaddr, int lineBufferSize )
  { super( in );
    is = this.in;
    this.conn = conn;
    ip = ipaddr;
    lxbuf = new byte[lxlen=lineBufferSize];
    ssl = is.toString().indexOf(".ssl.") > 0;
  }

  public boolean isSslStream()
  { return ssl;
  }
  
  public void cacheRequest( PrintStream logStream )
  { if( baos == null )
      baos = new ByteArrayOutputStream(1024);
    if( logStream != null )
      defaultLogStream = logStream;
  }

  public void logRequestCache()
  { logRequestCache(defaultLogStream);
  }
  
  public void logRequestCache( PrintStream logStream )
  { if( baos != null )
    { if( logStream != null )
    	{ String s = baos.toString();
    		if( s.trim().length() > 0 )
	        synchronized(this)
	        { logStream.println( "-------- request copy ---------" );
	          logStream.println( s );
	          logStream.println( "----- end of request copy -----" );
	        }
    	}
      baos.reset();
      baos = null;
    }
  }

  public void verboseYield( boolean byteCount,
                            boolean byteCopy,
                            boolean signalMark,
                            boolean includeTime )
  { this.byteCount = byteCount;
    this.byteCopy = byteCopy;
    this.signalMark = signalMark;
    this.includeTime = includeTime;
    if( !byteCount )
      lastTotal = totalRead = 0;
    if( yieldInfo == null )
      yieldInfo = new StringBuffer(100);
  }

  private StringBuffer setYieldInfo( String s )
  { yieldInfo.setLength(0);
    if( includeTime )
    {
      yieldInfo.append((new Date()).toString()).append(": ");
    }
    yieldInfo.append(s);
    return yieldInfo;
  }

  private void reportYieldInfo()
  { conn.addYieldInfo(yieldInfo.toString());
/*
System.out.println("reporting Yield Info");
System.out.println("byteCopy: "+byteCopy);
System.out.println("lineCopy: "+lineCopy);
System.out.println("byteCount: "+byteCount);
System.out.println("includeTime: "+includeTime);
System.out.println("signalMark: "+signalMark);
System.out.println("totalRead: "+totalRead);
*/
  }

  // returns the length of the properly terminated line
  // if the line is not terminated the length of the examined string
  // is returned as a negative value
  // examples: "abc" -> -3; "a\nc" -> 2; "\nbc" -> 1; "" -> 0
  private static int lineLength( byte b[], int off, int len )
  { int end = off + len;
    int i = off;
    while( i < end )
    { if( b[i++] == '\n' )
        return i - off;
    }
    return -len;
  }

  public boolean markSupported()
  { return true;
  }

  public void mark( int readlimit )
  { if( signalMark )
    { if( yieldInfo == null )
        yieldInfo = new StringBuffer(100);
      setYieldInfo("Mark @").append(Integer.toString(totalRead)).append("..+").append(Integer.toString(readlimit));
      reportYieldInfo();
    }
    if( mbuf == null )
      mbuf = new byte[readlimit];
    else if( readlimit > mbuf.length )
    { byte[] lb = mbuf;
      mbuf = new byte[readlimit];
      System.arraycopy(lb,0,mbuf,0,oPtr);
    }
    limit = readlimit;
    iPtr = oPtr = 0;
    unmarked = reread = false;
  }

  public void reset() throws IOException
  { if( signalMark )
    { if( yieldInfo == null )
        yieldInfo = new StringBuffer(100);
      setYieldInfo("Reset @"+totalRead);
      reportYieldInfo();
    }
    if( unmarked )
      throw new IOException("YieldingInputStream was reset past readlimit");
    oPtr = iPtr;
    reread = true;
  }

//  public long skip(long n) throws IOException
//  is inherited from superclass

  private int lastByte = -1;

  public int read() throws IOException
  {
//System.out.println("YieldingInputStream.read() "+totalRead);
    if( reread )
    { if( oPtr > 0 )
      { if( byteCount )
          totalRead++;
        return lastByte=mbuf[iPtr - oPtr--];
      }
      else
        reread = false;
    }
    while( true )
    { //int a = is.available();
      int a = 1;
      switch( a )
      {
/*
        case -1:
          setYieldInfo("End Of Stream");
          reportYieldInfo();
          return -1;
*/
        case 0:
if(!ssl){// !!! //
          if( (byteCount || byteCopy) && totalRead > lastTotal )
          { setYieldInfo("yielding read()");
            if( byteCount )
              yieldInfo.append(" @").append(Integer.toString(lastTotal=totalRead));
            if( byteCopy )
              yieldInfo.append("=").append(Integer.toString(lastByte));
            reportYieldInfo();
          }
          conn.yield();
          break;
}else if(sslDebug){System.err.println("YieldingInputStream.read() ignores available()==0 and attemps to read anyway");}
        default:
          if( (lastByte=is.read()) < 0 )
        	return -1;  
          byte b = (byte)lastByte;
          if( baos != null )
            baos.write(b);
          if( limit > 0 )
          { if( iPtr < limit )
            { mbuf[iPtr++] = b;
              oPtr++;
            }
            else
            { // we don't need to remember the bytes anymore
              limit = iPtr = oPtr = 0;
              unmarked = true;
            }
            if( byteCount )
              totalRead++;
  	      }
          return b;
      }
	}
  }

  public int read( byte[] b ) throws IOException
  { return read(b, 0, b.length);
  }

  public int read( byte[] b, int off, int len ) throws IOException
  { int a,l=b.length; // note that we allow for a NullPointerException here !
if(ssl && sslDebug)
System.err.println("YieldingInputStream.read([],"+off+","+len+")");
    if( l == 0 || len == 0 )
      return 0;
    if( off < 0 || len < 0 || l < off+len )
      throw new IndexOutOfBoundsException("YieldingInputStream read buffer dimension");
    if( reread )
    { if( oPtr > 0 )
      { // first report the buffered bytes
        System.arraycopy(mbuf,iPtr-oPtr,b,off,a=Math.min(oPtr,len));
        if( (oPtr-=a) == 0 )
        { reread = false;
          // let's see what is available in lxbuf
          int x =a;
          if( iLxb > oLxb )
          { System.arraycopy(lxbuf,oLxb,b,off+=x,a=Math.min(iLxb-oLxb,len-=x));
            if( baos != null )
              baos.write(b,off-x,a);
            oLxb += a;
          }
        }
        if( byteCopy )
          lastByte = b[off+a-1];
        lastTotal = totalRead;
        totalRead += a;
        return a;
      }
      reread = false;
    }
    // let's see what is available in lxbuf
    if( iLxb > oLxb )
    { System.arraycopy(lxbuf,oLxb,b,off,a=Math.min(iLxb-oLxb,len));
      if( baos != null )
        baos.write(b,off,a);
      if( (oLxb+=a) == iLxb )
      { // we are done
        if( byteCopy )
          lastByte = b[off+a-1];
        lastTotal = totalRead;
        totalRead += a;
        return a;
      }
    }
    while( true )
    { 
      if( (a=is.read(b,off,len)) < 0 )
    	return a;
      if( a > 0 )
      { if( baos != null )
          baos.write(b,off,a);
        if( byteCopy )
          lastByte = b[off+a-1];
        if( limit > 0 )
        { if( a > limit - iPtr )
          { // we don't need to remember the bytes anymore
            limit = iPtr = oPtr = 0;
            unmarked = true;
          }
          else
          { // we must buffer the read bytes in case of a reset()
            System.arraycopy(b,off,mbuf,iPtr,a);
            iPtr += a;
            oPtr += a;
          }
        }
        lastTotal = totalRead;
        totalRead += a;
        return a;
      }
      if( (byteCount || byteCopy) && totalRead > lastTotal )
      { setYieldInfo("yielding read([])");
        if( byteCount )
          yieldInfo.append(" @").append(Integer.toString(lastTotal=totalRead));
        if( byteCopy )
          yieldInfo.append(" ~~~ending~with: ").append(Integer.toString(lastByte));
        reportYieldInfo();
      }
      conn.yield();
    }
  }

  // reads next line from 'lxbuf'
  // reads a maximum of 'len' bytes
  // reads only less if prior LF is found
  // if 'lxbuf' is exhausted before either LF is found or 'len' bytes are read
  //  then the read bytes are moved to the target buffer 'b'
  //  and the next bytes are read into 'lxbuf' starting at 0
  // the state of 'lxbuf' is maintained in 'iLxb' and 'oLxb'
  // the mark buffer and it's state are neither altered nor considered by readLxBufLine()
  private int readLxBufLine( byte[] b, int off, int len ) throws IOException
  { int l, cnt = iLxb - oLxb;  // count of buffered bytes
    if( cnt == 0 )
      // reset the pointers for maximum top up
      iLxb = oLxb = l = 0;
    else
      l = lineLength(lxbuf,oLxb,Math.min(len,cnt));
    if( l > 0 || l+len == 0 )
    { // we found LF in the buffer ( l > 0 )
      // or we read the all 'len' bytes ( l < 0 )
      System.arraycopy(lxbuf,oLxb,b,off,l=Math.abs(l));
      if( baos != null )      // this works only because readLxBufLine is ALWAYS
        baos.write(b,off,l);  // called with 'b' being the EXTERNAL array
      oLxb += l;
      return l;
    }
    int done = -l;
    // here we have 'done' bytes in lxbuf starting at 'oLxb'
    // move the buffer content to the target buffer
    System.arraycopy(lxbuf,oLxb,b,off,done);
    if( baos != null )        // this works only because readLxBufLine is ALWAYS
      baos.write(b,off,done); // called with 'b' being the EXTERNAL array
    off += done;
    int a, max = Math.min(len-=done,lxlen);
    iLxb = oLxb = 0;
    while( true )
    { 
      if( (a=is.read(lxbuf,0,max)) < 0 )	////
    	return a;
      if( a > 0 )
      { // here we read the available bytes into lxbuf
        l = lineLength(lxbuf,0,a);
		if( l > 0 || l+len == 0 )
        { // we found LF in the buffer ( l > 0 )
          // or we read the all 'len' bytes ( l < 0 )
          System.arraycopy(lxbuf,0,b,off,(l=Math.abs(l))+iLxb);
          if( baos != null )      // this works only because readLxBufLine is ALWAYS
            baos.write(b,off,l);  // called with 'b' being the EXTERNAL array
          iLxb = a;
          return done + (oLxb=l);
        }
        // move the buffer content to the target buffer
        System.arraycopy(lxbuf,0,b,off,a);
        if( baos != null )      // this works only because readLxBufLine is ALWAYS
          baos.write(b,off,a);  // called with 'b' being the EXTERNAL array
        done += a;
        off += a;
        max = Math.min(len-=a,lxlen);
        iLxb = oLxb = 0;
      }
      if( byteCount && totalRead > lastTotal )
      { setYieldInfo("yielding read([])");
        if( byteCount )
          yieldInfo.append(" @").append(Integer.toString(lastTotal=totalRead));
        reportYieldInfo();
      }
      conn.yield();
    }
  }

  // Starting at the specified offset, reads into the given array of
  // bytes until all requested bytes have been read or a LF is
  // encountered, in which case the LF is read into the array as well.
  // Note:
  //    This implementation reads less then 'len' bytes if EOS is reached
  //      - only the next read(*) or readLine() will return -1
  //      - a following reset() will throw an IOException
  //    This behaviour can be altered by any derived subclasses
  // readLine() maintains the mark buffer and it's state
  public int readLine(byte b[], int off, int len) throws IOException
  { int a,l=b.length; // note that we allow for a NullPointerException here !
    if( l == 0 || len == 0 )
      return 0;
    if( off < 0 || len < 0 || l < off+len )
      throw new IndexOutOfBoundsException("YieldingInputStream read buffer dimension");
    int done = 0;
    if( reread )
    { if( oPtr > 0 )
      { // first report the bytes in the mark buffer up to and including LF
        a = Math.min(oPtr,len);
        if( (l=lineLength(mbuf,iPtr-oPtr,a)) > 0 )
        { System.arraycopy(mbuf,iPtr-oPtr,b,off,l);
          oPtr -= l;
          lastTotal = totalRead;
          totalRead += l;
          return l;
        }
        System.arraycopy(mbuf,iPtr-oPtr,b,off,a);
        if( (oPtr-=a) > 0 )
        { lastTotal = totalRead;
          totalRead += a;
          return a;
        }
        else
          reread = false;
        off += (done=a);
        len -= done;
      }
      else
        reread = false;
    }
    // we arrive here after 'done' unterminated buffered bytes have been processed
    // first we read everything into the target buffer 'b'
    if( (a=readLxBufLine(b,off,len)) < 0 )
    { if( done > 0 )
      { // we don't need to remember the bytes anymore
        limit = iPtr = oPtr = 0;
        unmarked = true;
        lastTotal = totalRead;
        totalRead += done;
        return done;      // we return first the byte count that was still read
      }
      return a;
    }
    if( limit > 0 )
    { if( done + a > limit - iPtr )
      { // we don't need to remember the bytes anymore
        limit = iPtr = oPtr = 0;
        unmarked = true;
      }
      else
      { // we must buffer the remaining read bytes in case of a reset()
        System.arraycopy(b,off,mbuf,iPtr,a);
        iPtr += a;
        oPtr += a;
      }
    }
    lastTotal = totalRead;
    totalRead += a + done;
    return a + done;
  }

  public int available() throws IOException
  {
    return oPtr + iLxb - oLxb + is.available();
  }

  public void close() throws IOException
  { is.close();
    logRequestCache( defaultLogStream );
  }

}

