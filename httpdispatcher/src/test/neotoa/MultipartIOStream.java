package us.breadex.neotoa;

import java.io.*;

// A class to aid in reading multipart/form-data from a ServletInputStream.
// It tests for multipart boundary and reports end of stream when reached.
// It can be called for the next multipart afterwards.
//

/*  Two methods:  read(buf[],off,len) and  readLine() are provided.
    Three private variables 'cnt', 'match' and 'done' keep track of the area
    where complete or incomplete boundary bytes possibly reside.
    The private variable 'dirty' is true when the boundary match is incomplete.
    The private variable 'eos' is true when the ServletInputStream is EOS.

      buf[match] contains first boundary candidate
      there are currently 'cnt' boundary candidates (starting @ 'match')
      there are currently 'done' bytes in the 'buf' that were already reported
*/

public class MultipartIOStream
{ private String pattern;
  private int pl;
  private String cmpPattern;
  private int cpl;
  private StringBuffer cmpPatternBuf;
  private int[] fwd;
  private HttpInputStream in;				// was ServletInputStream in;
  private boolean eos = false;
  private byte[] buf;
  private int cnt;       // # of bytes that could belong to boundary
  private int match;     // buffer index up to which the boundary does not start
  private int done;      // buffer index up to which all is already reported
  private boolean dirty;

  private String enc = null;

  public MultipartIOStream(HttpInputStream in,			// was ServletInputStream in,
                           String boundary)
  { this.in = in;
    pattern = "\r\n"+boundary;
    cmpPatternBuf = new StringBuffer();
    pl = pattern.length();
//System.out.println("pattern.length() "+pl);
    for( int i=0; i<pl-1; i++ )
    { char c = pattern.charAt(i);
      if( pattern.indexOf(c)==i )
        cmpPatternBuf.append(c);
    }
    cmpPattern = cmpPatternBuf.toString();
    cpl = cmpPattern.length();
    fwd = new int[cpl];
    String shortPattern = pattern.substring(0,pl-1);
    for( int i=0; i<cpl; i++ )
    { char c = cmpPattern.charAt(i);
      fwd[i] = pl-shortPattern.lastIndexOf(c)-1;
    }
    buf = new byte[8 * 1024];
    buf[match=0]=(byte)'\r';
    buf[1]=(byte)'\n';
    cnt=done=2;
    dirty = true;
  }

  public void setEncoding( String enc )
  { this.enc = enc;
  }

  public String getEncoding()
  { return enc;
  }

  // returns index where boundary match has occured
  // or index up to which no match could occur if to few
  // buffer bytes remain to allow positive match
  // also sets 'dirty' flag according to match type
  // requires precompiled pattern array 'fwd'
  //
  // the purpose of this method is to declare all bytes
  // prior to the returned index as not part of the boundary
  private int dirtyMatch( byte[] src, int off, int to )
  {
//if( to<=0 || off<0 )
//System.out.println("dirtyMatch(buf[],"+off+","+to+") !!!");
    int i,j,k,l;
//System.out.println("Matching bytes from "+off+" to "+to);
    i = pl + off;
    dirty = true;
    do
    { j = pl;
      if( (k=i) >= to )
      {
//        i -= pl;                              // new
        i = slowDirtyMatch(src,i,to);
//System.out.println("Early dirty EOB match @ "+i);
        return i;                            // was -n
      }
      do
      { k--; j--;
      }
      while( j>=0 && pattern.charAt(j)==(char)src[k] );
      if( j < 0 )
      { dirty = false;
//System.out.println("Clean match @ "+i);
        return i - pl;
      }
//      l = cmpPattern.indexOf((char)src[i-1]);
      char c = (char)src[i-1];              //
      l = cpl - 1;                          //
      while( l >= 0 )                       //
      { if( cmpPatternBuf.charAt(l) == c )  //
          break;                            //
        l--;                                //
      }                                     //
      // forward 'i' to the end of the possibly following boundary
      i += l>=0 ? fwd[l] : pl;
    }
    while( i<to );
    // push dirty match as far back as possible
    return slowDirtyMatch(src,i,to);  // index up to which no match has occurred
  }

  private int slowDirtyMatch( byte[] src, int i, int to )
  { int j = pl+1;
//System.out.print("-");
    do
    { i -= j-1;
      int c = (int)pattern.charAt(j=0);
      while( i<to && c!=src[i] )
        ++i;
      while( i<to && pattern.charAt(j)==(char)src[i] )
      { ++i; ++j; }
    } while( i<to );
//System.out.print("+");
    return i-j;           // index up to which no match has occured
  }


  // fills buffer 'buf' beginning @ 'match'+'cnt'
  // up to at least 'match'+'pl'
  // sets 'cnt', 'match', 'eos' & by calling dirtyMatch() 'dirty'
  private boolean fillBuffer()     // returns true if EOS
  throws IOException
  { int from;
    int to = match + cnt;
//    int plain = match;

//System.out.println("fillBuffer()");
//System.err.println("fillBuffer()");
    if( dirty == false )
      throw new IOException("Illegal MultipartIOStream context");
    if( eos )
    { if( cnt <= 0 )
        throw new IOException("MultipartIOStream underflow");
System.out.println("fillBuffer( buf,"+match+","+to+" )");
/////      match = dirtyMatch( buf, match , to );
/////System.out.println("dirtyMatch( buf,"+match+","+to+" )");
/////      return true;
    }
    else  /////
    {     /////
      if( match+cnt>=buf.length )
        throw new IOException("MultipartIOStream overflow");
    // fill up the buffer until it can hold at least a boundary
/////    if( !eos )
      do
      {
//System.err.println("in.read(buf,"+to+","+(buf.length-to)+")");
        if( (cnt=in.read( buf, from=to, buf.length - to )) < 0 )
        { eos = true;
//System.out.println("End of MultipartIOStream");
//System.err.println("End of MultipartIOStream");
          break;
        }
//System.err.println("filled buffer by "+cnt+" more bytes "+(from+cnt)+"/"+(pl+match));
      } while( (to=from+cnt) < pl+match );  // +match is new
    }     /////
    // test for boundary
    match = dirtyMatch( buf, match , to );
    if( dirty )
    { if( match > to )
        match = to;
      cnt = to - match;             // dirty match
    }
    else
    { cnt = to - match - pl;        // clean match
//System.out.println("clean match @ "+match);
    }
//System.out.println("/fillBuffer()");
//System.err.println("/fillBuffer()");
    return eos;
  }

  public int read( byte[] b, int off, int len ) throws IOException
  { int i;
    int predone = done;   // done before this method call
    int copied = 0;

//System.out.println("done: "+done+" match: "+match+" cnt: "+cnt);
    while( copied < len )
    {  // copy all releasable bytes from buffer
      if( (i=match-done) > len-copied )
        i = len-copied;
      System.arraycopy(buf,done,b,off,i);   /// fast copy replacement
      off += i;                             /// fast copy replacement
      done += i;                            /// fast copy replacement
      if( (copied=done-predone) == len )
      {
//System.out.println("released: "+copied);
        return copied;
      }

      i = 0;
      if( !dirty )                      // we have reached boundary
      { if( copied == 0 )
        { // prepare buffer for next multipart
//System.out.println("preparing next multipart @ "+match+" with "+cnt+" pending");
          match += pl;
/* */
          if( i<cnt )                                 /// fast copy replacement
            System.arraycopy(buf,match,buf,i,cnt-i);  /// fast copy replacement
/*
          while( i<cnt )
            buf[i++] = buf[match++];
*/
          done = match = 0;
          copied--;                 // report like end of stream
          dirty = true;
          fillBuffer();
        }
        return copied;
      }

      // shift the unreleased bytes to top of buffer
//if( cnt > 0 )
//System.out.println("shifting "+cnt+" bytes from "+match+" to "+i);
/* */
      if( i<cnt )                                 /// fast copy replacement
        System.arraycopy(buf,match,buf,i,cnt-i);  /// fast copy replacement
/*
      while( i<cnt )
        buf[i++]=buf[match++];
*/
      match = done = 0;
      predone = -copied;
      if( fillBuffer() )
      {
//System.out.println("read(,,) returns "+copied+" bytes out of "+len);
        return copied==0 ? -1 : copied;
      }
//System.out.println("looping after "+copied);
//System.out.println("done: "+done+" match: "+match+" cnt: "+cnt);
    }
System.out.println("we should never arrive here");
    return copied;
  }

  // Reads the next line of input.
  // (could be boundary or META data)
  // Returns null to indicate the end of stream.
  //
  public String readLine() throws IOException
  { String line;
    int i, j, rlen;
    int predone = done;

//System.err.println("MultipartIOStream.readLine() "+in);
    if( match <= done && dirty )            // new '&& dirty'
    { fillBuffer();
//System.out.println("match: "+match+" dirty: "+dirty+" done: "+done+" cnt: "+cnt);
//System.err.println("match: "+match+" dirty: "+dirty+" done: "+done+" cnt: "+cnt);
    }
    while( done < buf.length )
    { i=0;
      if( (rlen=match-done)<=0 && !dirty )
      { j = match + pl;
/* */
        if( i<cnt )                                 /// fast copy replacement
          System.arraycopy(buf,j,buf,i,cnt-i);      /// fast copy replacement
/*
        while( i<cnt )
          buf[i++]=buf[j++];
*/
        match = done = 0;
        dirty = true;
        fillBuffer();
//System.out.println("boundary----------------------------");
//System.err.println("boundary----------------------------");
        return null;             // we have reached boundary
      }
      // test for end of line
//System.out.println("test for end of line @ "+done+" while "+i+"<"+rlen);
//System.err.println("test for end of line @ "+done+" while "+i+"<"+rlen);
      while( i++ < rlen )
        if( buf[done++] == 10 )
        { line = new String(buf,predone,done-predone-2,"ISO-8859-1");
//System.out.println(line);
          return line;           // found EOL before match
        }
      // test if match starts with CRLF
//System.out.println("test if match starts with CRLF");
//System.err.println("test if match starts with CRLF");
      if( match<buf.length-2 && buf[match]==13 && buf[match+1]==10 )
      { done+=2;
        line = new String(buf,predone,done-predone-2,"ISO-8859-1");
//System.out.println(line);
        return line;             // found EOL at match
      }
      // test if unreleased byte area starts at top of buffer
      if( predone == 0 )         // yes
{
//System.out.println("match: "+match+" dirty: "+dirty+" done: "+done+" cnt: "+cnt);
//System.out.println(new String(buf,0,cnt,"ISO-8859-1"));
        throw new IOException("Multipart Line Too Long.");
}
      i = 0;
      j = predone;
      if( dirty )
      { // shift the unreleased bytes to top of buffer
//System.out.println("end-of-buffer----------------------");
//System.err.println("end-of-buffer----------------------");
/* */
        if( j<match+cnt )                                 /// fast copy replacement
          System.arraycopy(buf,j,buf,i,match+cnt-j);      /// fast copy replacement
/*
        while( j<match+cnt )
          buf[i++] = buf[j++];
*/
        cnt += match-predone;
//line = new String(buf,predone,done-predone,"ISO-8859-1");
//System.out.println(line);
        predone = done = match = 0;
      }
      else   // we have reached boundary and should have returned at first test
      { throw new IOException("Internal error: 0");
      }
      fillBuffer();
    }
System.err.println("MultipartIOStream: ----------we should never arrive here------------");
    // we should never arrive here
    return null;
  }
}
