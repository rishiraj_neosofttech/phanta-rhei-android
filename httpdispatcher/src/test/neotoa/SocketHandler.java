package us.breadex.neotoa;

import java.io.*;
import java.util.*;
import java.net.*;
//import java.text.*;

/*
FileServer.copyStream
*/
class SocketHandler implements Runnable
{ private Thread thisThread;

  private static final int PR_GETTING_STREAMS = 0;
  private static final int PR_READING_REQ = 1;
  private static final int PR_EMPTY_REQ = 2;
  private static final int PR_MALFORMED_REQ = 3;
  private static final int PR_MISSING_1_1_HOST = 5;
  private static final int PR_IO_PROBLEM = 6;
  private static final int PR_WRONG_HTTP = 7;
  private static final int PR_EXCEPT = 8;

  private static final String getUri = "/home.html";
  
  private Socket socket;
  private HttpDispatcher serve;
  private String matchName;

  private HttpInputStream in;				// was ServletInputStream in;
  private ServletOutputStream out;		
  private InputStream socketIn;
  private OutputStream socketOut;
  private int contentLength = -1;
  private boolean finalOut = true;
  private boolean newError;

  private int ua_ndx = -1;
  private String reqUserAgent = "";
  private static String[] reqUserAgents = new String[2];
  private static String[] cTypes = new String[2];
  private static long[] timestamps = { -2L, -2L };
  private static ByteArrayOutputStream[] streams = new ByteArrayOutputStream[2];
  private String reqCookie = null;
  private String resCookie = null;
  private static String[] reqCookies = new String[2];
  private static String[] resCookies = new String[2];

  private String virtualDomain = null;

  private byte[] lineBytes = new byte[4096];

  /// Constructor.
  public SocketHandler( Socket socket, HttpDispatcher serve )
  {
  	// Save arguments.
  	this.socket = socket;
  	this.serve = serve;

    // Get IO streams
    try
    { socketIn = socket.getInputStream();
      socketOut = socket.getOutputStream();
    }
    catch ( IOException e )
    { problem( PR_GETTING_STREAMS, e.getMessage(), SC_BAD_REQUEST );
    }
    newError = true;

    // Start a separate thread to read and handle the request.
    thisThread = new Thread( this );
    thisThread.start();
  }


  private String reqMethod = null;
  private String reqUriPath = null;


    // Methods from Runnable.

  private String reqProtocol = null;
  private boolean oneOne;		// HTTP/1.1 or better
  private boolean reqMime;
  private boolean secure;

  public int requestCount = 0;
  public String sockIp = null;


  private StringBuffer yieldReport = null;
  private int yieldReportDelay = 10000;   // 10 seconds
  private long lastYieldReport = 0L;

  public void yield()
  { long now = System.currentTimeMillis();
    if( lastYieldReport != 0L )
    { int diff = (int)(now - lastYieldReport);
      if( Math.abs(diff) > yieldReportDelay )
      { String s = null;
        if( yieldReport == null )
          s = "no Yield Report so far";
        else if( yieldReport.length() > 0 )
        { s = yieldReport.append("Yield Report:\n").toString();
          yieldReport = new StringBuffer();
        }
        lastYieldReport = now;
      }
    }
    Thread.yield();
  }

  public void setYieldReportDelay( int seconds )
  { yieldReportDelay = Math.abs( seconds ) * 1000;
  }

  public void addYieldInfo( String info )
  { if( info != null )
    { if( yieldReport == null )
        yieldReport = new StringBuffer(info);
      else
        yieldReport.append(info);
    }
  }

  public void run()
  {	
    InetAddress ia = socket.getInetAddress();
    sockIp = ia.toString();
//    intra = Token.isPrivateIP( sockIp ); // we should reject all ips except for localhost
  	
  	if( reqMethod == null )
	{ int port = socket.getLocalPort();
      YieldingInputStream yis = new YieldingInputStream(socketIn,this,sockIp);
      secure = false;
      yis.verboseYield(true,true,true,true);
      yis.cacheRequest(null);   // cache all requests with "autolog on closing" to the default PrintStream
      in = new HttpInputStream( yis, this );
      out = new ThreadOutputStream( socketOut, this );
	}

    String firstLine = getLine();
System.out.println("firstLine: "+firstLine);

    while( true )
    { parseRequest(firstLine);
      firstLine = null;
      if( finalOut )
      { try { in.close(); }
        catch ( IOException e ) {}
          
     	  try { out.close(); }
        catch ( IOException e ) {}
          
   	    try
        { socket.close();
        }
  	    catch ( IOException e )
        { /* ignore */
System.out.println("IOException on socket "+socket);
        }
        break;
      }
    }
  }

  // returns a string representing the read line WITHOUT the terminating LF or CR LF
  // in case of EOS or an IOException the read bytes up to the exception are returned as if they were terminated by LF !!!
  // only EOS or an IOException without any read bytes result in the return of a null string
  private String getLine()
  { int len=0,off=0;
    int lbl = lineBytes.length;
  	String line="";

  	try
  	{ while( true )
      { if( (len = in.readLine( lineBytes, 0, lbl )) > 0 )
	      while( len > 0 && (lineBytes[len-1] == 13 || lineBytes[len-1] == 10) )
	        --len;
  	    if( len > 0 && lineBytes[0] == 13 )
	    { ++off; --len; }
        if( len < 0 )
        { return line.length() > 0 ? line : null;
        }
	      line += new String( lineBytes, off, len );
        if( len < lbl )
          return line;
      }
    }
  	catch ( IOException e )
  	{ if( requestCount == 0 )
        problem( PR_READING_REQ, e.getMessage(), SC_BAD_REQUEST );
      if( len == 0 )
        line = null;
  	}
    return line;
  }

  private void parseRequest(String firstLine)
  { String line;
    String ctype = null;
    boolean is_post = false;
    boolean is_get = false;
    boolean no_sess_post = true;
    boolean no_sess_get = true;

    // Read the first line of the request.
		line = firstLine == null ? getLine() : firstLine;

    // Ignore first empty request line
    if( line != null && line.length() == 0 )
    	line = getLine();
    if( line == null || line.length() == 0 )
    { if( requestCount == 0 )
        problem( PR_EMPTY_REQ, SC_BAD_REQUEST );
      return;
    }

    String[] tokens = line.split(" ");
    switch( tokens.length )
    {
      case 2:
		// Two tokens means the protocol is HTTP/0.9.
		    reqProtocol = "HTTP/0.9";
		    oneOne = false;
		    reqMime = false;
		    break;
      case 3:
  		  	reqProtocol = reqMethod==null ? tokens[2] : tokens[0];
  		  	reqMethod = tokens[0];
  		  	reqUriPath = tokens[1];
		  	no_sess_post = is_post = reqMethod.equalsIgnoreCase("POST");
		  	no_sess_get = is_get =  reqMethod.equalsIgnoreCase("GET");
		  	reqMime = true;

        // Read the rest of the lines.
        
  		  while( true )
  		  { line = getLine();
  		    if( line == null || line.length()==0 )
  			    break;
  		    int colonBlank = line.indexOf( ": " );
  		    if( colonBlank != -1 )
  		    { String name = line.substring( 0, colonBlank );
    			  String value = line.substring( colonBlank + 2 );
  			  	name = name.toLowerCase();
  				  if( name.startsWith( "content-type" ) )
  				  { int i = value.indexOf( "charset=" );
  				    if( i>0 )
  				    { charsetIn = charsetOut = value.substring( i + 8 );
  				      ctype = value.substring(0,i);
  				    }
  				    else
  				      ctype = value;
  				  }
  				  else if( name.startsWith( "content-length" ) )
  				    contentLength = parseInt( value, 10, -1 );
  				  else if( name.startsWith( "cookie" ) )
  				  { Enumeration en = new StringTokenizer( value, ";" );
  				    while( en.hasMoreElements() )
  				    { String nv = (String) en.nextElement();
  				      int eq = nv.indexOf( '=' );
  				      String cn, cv;
  			    	  if( eq == -1 )
  						  { cn = nv;
  						    cv = "";
  						  }
  						  else
  						  { cn = nv.substring( 0, eq );
  						    cv = nv.substring( eq + 1 );
  						  }
  						  int i = 0;
  						  while( cn.length()>i && cn.charAt(i)==' ' )
  						    ++i;
  						  if( (cn=cn.substring(i)).toLowerCase().equals("prsn") )
  						  { if( is_get && !cv.equals( reqCookie) )
  							  cv = null;
  						  	reqCookie = cv;
  						    no_sess_post = no_sess_get = false;
  						  }
  				    }
  				  }
  				  else if( name.equals( "user-agent" ) )
		          { if( reqUserAgents[0].length() == 0 )
		                reqUserAgents[ua_ndx=0] = value;
		              else if( value.equalsIgnoreCase( reqUserAgents[0]) )
		                ua_ndx = 0;
		              else if( reqUserAgents[1].length() == 0 )
		                reqUserAgents[ua_ndx=1] = value;
		              else if( value.equalsIgnoreCase( reqUserAgents[1]) )
		                ua_ndx = 1;
		              else
		                ua_ndx = -1;
		            }
  				  else if( name.equals( "host" ) )
            {  virtualDomain = value;
            }
            int req_ndx = reqHeaderNameNdx( name );
            if( req_ndx >= 0 )
              reqHeaderValue[req_ndx] = value;
  			}
  		  }
        if( reqCookie == null || ua_ndx < 0 )
        { if( is_post )
			no_sess_post = true;
          else if( is_get )
            no_sess_get = true;
        }
        if( ua_ndx >= 0 )
        { reqUserAgent = reqUserAgents[ua_ndx];
          reqCookies[ua_ndx] = reqCookie;
          cTypes[ua_ndx] = ctype;
        } 
        else
          reqCookie = null;
  		  in.setTotalExpected( contentLength );
  		  break;
      default:
		    problem( PR_MALFORMED_REQ, SC_BAD_REQUEST );
  		  return;
    }

    if( !oneOne )
    { problem( PR_WRONG_HTTP, SC_HTTP_VERSION_NOT_SUPPORTED );
      return;
    }
	
	    // Check Host: header in HTTP/1.1 requests.
    if ( virtualDomain == null )
    { problem( PR_MISSING_1_1_HOST, SC_BAD_REQUEST );
      return;
    }

    virtualDomain = normalizeDomain(virtualDomain,socket.getLocalPort());
    
      // Split off leading scheme, host, and port part if present
      // Perform redundancy checks if leaders are present
    if( reqUriPath.indexOf(':') >= 0 )
    { int port = socket.getLocalPort();
      String p = parseUri(reqUriPath,1);
      String pt = parseUri(reqUriPath,3);
      if( p!=null && pt!=null && (( p.equalsIgnoreCase("http") && port!=parseInt(pt,10,-1) )))
  	  { problem( PR_MALFORMED_REQ, SC_BAD_REQUEST );
	      return;
  	  }
      String h = parseUri(reqUriPath,2);
      if( h != null )
      { if( !h.equalsIgnoreCase(virtualDomain) )
      	{ problem( PR_MALFORMED_REQ, SC_BAD_REQUEST );
	        return;
        }
        else
          virtualDomain = normalizeDomain(p,port);
	    }
      if( pt != null && port != parseInt(pt,10,-1) )
  	  { problem( PR_MALFORMED_REQ, SC_BAD_REQUEST );
		    return;
		  }
      String rup = parseUri(reqUriPath,4);
      reqUriPath = rup;
    }
	    // Split off fragment, if any.
    int qmark = reqUriPath.indexOf('#');
    if( qmark != -1 )
      reqUriPath = reqUriPath.substring( 0, qmark );

	    // Split off query string, if any.
    qmark = reqUriPath.indexOf('?');
    if( qmark != -1 )
      reqUriPath = reqUriPath.substring( 0, qmark );

	    // Decode %-sequences.
    try
    { reqUriPath = URLDecoder.decode( reqUriPath, "UTF-8" );
    }
    catch( Exception e )
    { problem( -1, SC_BAD_REQUEST );
      return;
    }

    if( no_sess_get || ua_ndx < 0 )
    { problem( -1, SC_FORBIDDEN );
      return;
    }

    if( reqUriPath.trim().length() == 0 )
    { problem( -1, SC_BAD_REQUEST );
      return;
    }

    if( is_post )
    { if( ctype.startsWith("multipart/form-data") )
      { // possibly special bounce POST
    	if( reqUriPath.equals("/") )
    	{ // bounce
    	  ByteArrayOutputStream baos = streams[ua_ndx];
    	  int len = 0;
    	  MultipartRequest mpr = new MultipartRequest( this, baos, 128 * 1028 );	
    	  try
    	  { mpr.readRequest();
    		len = baos.size(); 
      	  	if( len <= 0 )
      	  	{ problem( -1, SC_BAD_REQUEST );
      	  	  return;
      	  	}
      	  	long now = System.currentTimeMillis();
      	  	String cookie = now + "";
      	  	addCookie(cookie);
      	    String timestamp = HttpDispatcher.gmtFormatter.format(new Date(now));
      	    setHeader( "Date", timestamp );
      	    timestamps[ua_ndx] = now;
    		sendRedirect(getUri);
    	  }
    	  catch( IOException e )
    	  {
    		  
    	  }    	  
    	}
    	else
        { problem( -1, SC_FORBIDDEN );
        }
        return;
      }
    	
    }    
    if( no_sess_post || no_sess_get )
    { problem( -1, SC_FORBIDDEN );
      return;
    }

    if( is_get && getUri.equals(reqUriPath) )
    { ByteArrayOutputStream baos = streams[ua_ndx];
	  int len = baos.size();
	  if( len == 0 )
	  { String etag = getHeader("if-none-match");
	  	try
	  	{ if( reqCookie.equals(etag) )
		  { sendError(SC_NOT_MODIFIED);
		    return;
		  }
		  long ref = getDateHeader("if-modified-since", -1L);
		  if( ref <= timestamps[ua_ndx] )
		  { sendError(SC_NOT_MODIFIED);
		    return;
		  }
		  sendError(SC_NOT_FOUND);
	  	}
	  	catch( IOException e )
	  	{}
		return;
	  }
	  String stored = baos.toString();
	  baos.reset();
	  setStatus(SC_OK);
	  setContentType("text/html");
	  setContentLength(len);
	  setHeader("Etag", resCookie);
    	// sending of the content is still missing
    }
    
    matchName = reqUriPath;
    
/*
    if( matchName.equalsIgnoreCase("/favicon.ico") )
    { String fn = getRealPath( matchName );
      File f = new File(fn);
      if( f.exists() && !f.isDirectory() )
      { try
    	{ FileInputStream fis = new FileInputStream( f );
    	  setStatus( HttpServletResponse.SC_OK );
    	  setContentType("image/x-icon");
    	  out = getOutputStream();
    	  FileServer.copyStream(fis, out);
          out.close();
    	  return;
    	}
    	catch( IOException ie )
    	{
System.err.println(ie.getMessage());
    	}
      }
    }
*/   
 
    Object target = serve.targets.get( reqUriPath );
    if( target != null )
    { prepareForIPC(reqUriPath,this);
    }
/*
	Servlet servlet = lookupByUri( reqUriPath );
	if( servlet != null )
	  runServlet( (HttpServlet) servlet );
*/
  }

  private void prepareForIPC( String id, SocketHandler sock_hdl )
  { requestCount++;
  	String par = getParameter("json");
  	
  	setHeader( "Accept-Ranges", "none" );
  	// Set default response fields.
    String timestamp = HttpDispatcher.gmtFormatter.format(new Date());
    setStatus( SC_OK );
    setHeader( "Date", timestamp );
    setHeader( "Server", serve.getServerInfo() );
    String res_body = null;
    try
    { res_body = performIPC( id, par, this );
      if( res_body == null )
      { // report IPC error
        return;
      }
      setContentType("text/plain");
      out.print(res_body);
      out.flush();
      out.close();
   }
    catch ( IOException e )
    { problem( PR_IO_PROBLEM, e.toString(), SC_BAD_REQUEST );
    }
    catch ( Exception e )
    { problem( PR_EXCEPT, e.toString(), SC_INTERNAL_SERVER_ERROR );
System.err.println(e.toString());
e.printStackTrace(System.err);
    }
  }

  private String performIPC( String id, String cmd, SocketHandler sock_hdl )
  { // still TODO
	return null;
  }
  
  private void problem( int logCode, int resCode )
  { setStatus( resCode );
	  probl(resCode,logCode>=0);
  }

  private void problem( int logCode, String eStr, int resCode )
  { setStatus( resCode );
  	probl(resCode,logCode>=0);
  }

  private void probl( int resCode, boolean inclMsg )
  { if( newError )
    { newError = false;
  	  try
	  { sendError( resCode,inclMsg );
	  }
  	  catch ( IOException e ) { /* ignore */ }
    }
  }

    // Methods from ServletRequest.

  private String charsetIn = null;

    /// Returns the size of the request entity data, or -1 if not known.
    // Same as the CGI variable CONTENT_LENGTH.
  public int getContentLength()
  {	return getIntHeader( "Content-length", -1 );
  }

    /// Returns the IP address of the agent that sent the request.
    // Same as the CGI variable REMOTE_ADDR.
  public String getRemoteAddr()
  { return socket.getInetAddress().toString();
  }

    /// Returns the fully qualified host name of the agent that sent the
    // request.
    // Same as the CGI variable REMOTE_HOST.
  public String getRemoteHost()
  { return socket.getInetAddress().getHostName();
  }

    /// Returns an input stream for reading request data.
    // @exception IllegalStateException if getReader has already been called
    // @exception IOException on other I/O-related errors
  public HttpInputStream getInputStream() throws IOException
  { if( contentLength > 0 )
	  in.setTotalExpected( contentLength );
	return in;
  }

    /// Returns a buffered reader for reading request data.
    // @exception UnsupportedEncodingException if the character set encoding isn't supported
    // @exception IllegalStateException if getInputStream has already been called
    // @exception IOException on other I/O-related errors
  public BufferedReader getReader() throws UnsupportedEncodingException
  { if( charsetIn == null )
	  return new BufferedReader( new InputStreamReader( in ));
  	return new BufferedReader( new InputStreamReader( in, charsetIn ));
  }


    // Methods from HttpServletRequest.

    /// Returns the method with which the request was made. This can be "GET",
    // "HEAD", "POST", or an extension method.
    // Same as the CGI variable REQUEST_METHOD.
  public String getMethod()
  {	return reqMethod;
  }

    /// Returns the part of the request URI that referred to the servlet being
    // invoked.
    // Analogous to the CGI variable SCRIPT_NAME.
  public String getServletPath()
  {
  	// In this server, the entire path is regexp-matched against the
	  // servlet pattern, so there's no good way to distinguish which
  	// part refers to the servlet.
	return reqUriPath;
  }

    /// Returns the value of a header field, or null if not known.
    // Same as the information passed in the CGI variabled HTTP_*.
    // @param name the header field name
  public String getHeader( String name )
  { if( name == null)
	  return matchName;
  	int i = reqHeaderNameNdx( name.toLowerCase() );
  	if ( i == -1 )
  	  return null;
  	return reqHeaderValue[i];
  }

    /// Returns the value of an integer header field.
    // @param name the header field name
    // @param def the integer value to return if header not found or invalid
  public int getIntHeader( String name, int def )
  { String val = getHeader( name );
    return val == null ? def : parseInt( val, 10, def );
  }

    /// Returns the value of a long header field.
    // @param name the header field name
    // @param def the long value to return if header not found or invalid
  public long getLongHeader( String name, long def )
  {	String val = getHeader( name );
  	if( val == null )
  	  return def;
  	try
	{ return Long.parseLong( val );
	}
  	catch ( Exception e )
  	{ return def;
  	}
  }

    /// Returns the value of a date header field.
    // @param name the header field name
    // @param def the date value to return if header not found or invalid
  public long getDateHeader( String name, long def )
  {	String val = getHeader( name );
  	if( val == null )
  	  return def;
  	try
	{ return serve.gmtFormatter.parse( val ).getTime();
	}
  	catch ( Exception e )
  	{ return def;
  	}
  }

    // Session stuff.  Not implemented, but the API is here for compatibility.

  public String getUserAgent()
  { return reqUserAgent;	
  }
  

    // Methods from ServletResponse.

  private String charsetOut = null;

    /// Sets the content length for this response.
    // @param length the content length
  public void setContentLength( int length )
  { setIntHeader( "Content-length", resContentLength = length );
  }

    /// Sets the content type for this response.
    // @param type the content type
  // note that setContentType("") will not set the header !!!
  // but setContentType("") will allow getOutputStream() to follow
  public void setContentType( String type )
  { int i = type.indexOf( "charset=" );
  	if( i > 0 )
	  charsetOut = type.substring( i + 8 );
  	i = type.indexOf( ";" );
	int ndx = type.indexOf(",");
	if( reqMime && type.length() >0 && ndx>0 )
	  type = type.substring(0,ndx) + "/" + type.substring(ndx+1);
    if( type.length() > 0 )
  	  setHeader( "Content-type", type );
  }

    /// Returns an output stream for writing response data.
  public OutputStream getOutputStream()
  {	return out;
  }

    /// Returns the character set encoding used for this MIME body.  The
    // character encoding is either the one specified in the assigned
    // content type, or one which the client understands.  If no content
    // type has yet been assigned, it is implicitly set to text/plain.

    // above should refer to REQUEST encoding !!!
  public String getCharacterEncoding()
	{	return charsetIn;
	}


    // Methods from HttpServletResponse.

    /// Adds the specified cookie to the response.  It can be called
    // multiple times to set more than one cookie.
  public void addCookie( String cookie )
	{ if( cookie == null || ua_ndx < 0 )
      return;
    resCookies[ua_ndx] = resCookie = cookie;
	}

    /// Checks whether the response message header has a field with the
    // specified name.
  public boolean containsHeader( String name )
  { int i = resHeaderNameNdx( name.toLowerCase() );
    return resHeaderName[i] != null;
  }

    // Status codes.
  private static final int SC_OK = 200;
  private static final int SC_NO_CONTENT = 204;
  public static final int SC_MOVED_TEMPORARILY = 302;
  public static final int SC_SEE_OTHER = 303;
  public static final int SC_NOT_MODIFIED = 304;
  private static final int SC_BAD_REQUEST = 400;
  private static final int SC_FORBIDDEN = 403;
  private static final int SC_NOT_FOUND = 404;
  private static final int SC_METHOD_NOT_ALLOWED = 405;
  private static final int SC_REQUEST_TIMEOUT = 408;
  private static final int SC_LENGTH_REQUIRED = 411;
  private static final int SC_REQUEST_ENTITY_TOO_LARGE = 413;
  private static final int SC_REQUEST_URI_TOO_LONG = 414;
  private static final int SC_UNSUPPORTED_MEDIA_TYPE = 415;
  private static final int SC_INTERNAL_SERVER_ERROR = 500;
  private static final int SC_NOT_IMPLEMENTED = 501;
  private static final int SC_HTTP_VERSION_NOT_SUPPORTED = 505;

    /// Sets the status code and message for this response.
    // @param resCode the status code
    // @param resMessage the status message
  public void setStatus( int resCode, String resMessage )
  {	this.resCode = resCode;
	this.resMessage = resMessage;
  }

    /// Sets the status code and a default message for this response.
    // @param resCode the status code
  public void setStatus( int resCode )
	{ switch( resCode )
	  { case SC_OK:
          setStatus( resCode, " Ok" ); break;
	    case SC_MOVED_TEMPORARILY:
        	setStatus( resCode, " Moved temporarily" ); break;
	    case SC_SEE_OTHER:
        	setStatus( resCode, " See other" ); break;
	    case SC_NOT_MODIFIED:
          setStatus( resCode, " Not modified" ); break;
	    case SC_BAD_REQUEST:
          setStatus( resCode, " Bad request" ); break;
	    case SC_NO_CONTENT:
          setStatus( resCode, " No content" ); break;
	    case SC_FORBIDDEN:
          setStatus( resCode, " Forbidden" ); break;
	    case SC_NOT_FOUND:
          setStatus( resCode, " Not found" ); break;
	    case SC_METHOD_NOT_ALLOWED:
        	setStatus( resCode, " Method not allowed" ); break;
	    case SC_LENGTH_REQUIRED:
        	setStatus( resCode, " Length required" ); break;
	    case SC_REQUEST_ENTITY_TOO_LARGE:
        	setStatus( resCode, " Request entity too large" ); break;
	    case SC_REQUEST_URI_TOO_LONG:
        	setStatus( resCode, " Request URI too large" ); break;
	    case SC_UNSUPPORTED_MEDIA_TYPE:
        	setStatus( resCode, " Unsupported media type" ); break;
	    case SC_INTERNAL_SERVER_ERROR:
        	setStatus( resCode, " Internal server error" ); break;
	    case SC_NOT_IMPLEMENTED:
        	setStatus( resCode, " Not implemented" ); break;
	    case SC_HTTP_VERSION_NOT_SUPPORTED:
        	setStatus( resCode, " HTTP version not supported" ); break;
	    default:
          setStatus( resCode, "" ); break;
	  }
	}

  public int getStatus()
  {	return resCode;
  }

  private String reqHeaderNameList = "content-type 0 content-length 1 cookie 2 user-agent 3 host 4 if-none-match 5 if-modified-since 6";
  private String reqHeaderIndexes =  "0123456";
  private int reqHeaderMax =  reqHeaderIndexes.length();

  private String[] reqHeaderValue = new String[reqHeaderMax];

  private int reqHeaderNameNdx( String nm )
  { return reqHeaderIndexes.indexOf(reqHeaderNameList.charAt(reqHeaderNameList.indexOf(nm) + nm.length() + 1));
  }
  
  private String resHeaderNameList = "cache-control 0 expires 1 content-type 2 content-length 3 location 4 accept-ranges 5 date 6 server 7";
  private String resHeaderIndexes =  "01234567";
  private int resHeaderMax =  resHeaderIndexes.length();

  private String[] resHeaderKey = new String[resHeaderMax];
  private String[] resHeaderName = new String[resHeaderMax];
  private String[] resHeaderValue = new String[resHeaderMax];
  private int resHeaderCount = 0;

  private int resHeaderNameNdx( String nm )
  { return resHeaderIndexes.indexOf(resHeaderNameList.charAt(resHeaderNameList.indexOf(nm) + nm.length() + 1));
  }
  
  private int resContentLength = -1;
  private int resCode = -1;
  private String resMessage = "";
  
    /// Sets the value of a header field.
    // @param name the header field name
    // @param value the header field value
    // if value == null the header fields are removed
  public void setHeader( String name, String value )
  { setHeader(name,name.toLowerCase(),(Object)value);
  }

  private void setHeader( String name, String n, Object value )
  { int nm_ndx = resHeaderNameNdx( n );
    if( nm_ndx < 0 )
      return;
    if( value == null )
    { resHeaderKey[nm_ndx] = null;
      if( resHeaderName[nm_ndx] != null )
        resHeaderCount--;
      resHeaderName[nm_ndx] = null;
    }
    else
    { if( resHeaderName[nm_ndx] == null )
        resHeaderCount++;
      resHeaderName[nm_ndx] = name;
    }
    resHeaderValue[nm_ndx] = (String)value;
  }

  public void setIntHeader( String name, int value )
  { setHeader(name,name.toLowerCase(),Integer.toString(value));
  }


 /*
    /// Sets the value of a date header field.
    // @param name the header field name
    // @param value the header field date value
  public void setDateHeader( String name, long value )
  {
    setHeader( name, webFormatter.format( new Date(value) ) );
//	setHeader( name, PrcsDateFormat.formatRFC1123_byte(value) );
  }
*/
    /// Sets the value of the header fields to force no-caching.
  public void forceNoCache()
  {	setHeader("Cache-control", "no-cache, max-age=0, must-revalidate");
    setHeader("Expires", "Tue, 01 Jan 1980 00:00:00 GMT");
  }

  private int headerCount = -2;
  private int headerMax = 0;
  private boolean connectionHdr;

    /// Returns one line per call
    /// starts with the status line 
    /// followed by response headers to the output stream.
  public String getHeaderStr()
  { StringBuffer s;
    if( headerCount > headerMax || reqProtocol == null )
    { return null;
    }
    // make sure we include Connection header
    if( headerCount == headerMax )
    { if( connectionHdr )
        return null;
      ++headerCount;
      return "Connection: close";
    }
    // return status line as first line
    if( ++headerCount == -1 )
    { if( resCode != SC_OK )
      { setHeader( "Content-type", "content-type", null );
      }
      headerMax = resHeaderCount + (resCookie==null ? -1 : 0);
      connectionHdr = false;
      s = new StringBuffer(reqProtocol);
      s.append(" ").append(resCode).append(resMessage);
      return s.toString();
    }
    if( !reqMime )
      return null;
    String value = "";
    if( headerCount < resHeaderCount )
    { String name = null;
      while( headerCount < resHeaderMax )
      { name = resHeaderName[headerCount];
      	if( name == null )
        { resHeaderCount++;
          headerCount++;
        }
        else
        { value = resHeaderValue[headerCount];
          break;
        }
      }
      if( name == null )
        return null;                                // this should never happen
      if( name.equalsIgnoreCase("Connection") )
      { if( value.equalsIgnoreCase("close") )
        { connectionHdr = true;
        }
      }
      s = new StringBuffer(name);
      s.append(": ");
  		if( value != null )	// just in case
        s.append(resHeaderValue[headerCount]);
      return s.toString();
    }

    if( ua_ndx < 0 )
      return null;
    long ex = (new Date()).getTime() + 31300000000L;
    s = new StringBuffer("Set-cookie: prsn=").append(resCookie);
//    s.append("; expires=").append(serve.gmtFormatter.format(new Date(ex)));
    s.append("; path=/");
    return s.toString();
  }
  
/*
  public byte[] getHeaderBytes( String name )
  { int i = resHeaderNameNdx( name.toLowerCase() ); 
    return i < 0 ? null : (byte[])resHeaderValue[i];
  }
*/

    /// Writes an error response using the specified status code and message.
    // @param resCode the status code
    // @param resMessage the status message
    // @exception IOException if an I/O error has occurred
  public void sendError( int resCode, String resMessage ) throws IOException
  { if( resMessage != null )
    { setStatus( resCode, resMessage );
      realSendError(true);
    }
    else
    { setStatus( resCode );
	  realSendError(false);
    }
  }

    /// Writes an error response using the specified status code and a default
    // message.
    // @param resCode the status code
    // @exception IOException if an I/O error has occurred
  public void sendError( int resCode ) throws IOException
  { setStatus( resCode );
	realSendError(true);
  }

  public void sendError( int resCode, boolean inclMsg ) throws IOException
  { setStatus( resCode );
	realSendError(inclMsg);
  }

  private void realSendError(boolean inclMsg) throws IOException
  { boolean sndMsg = inclMsg && resCode >= 200 && resCode != SC_NO_CONTENT && resCode != SC_NOT_MODIFIED;
    StringBuffer sb = sndMsg ? new StringBuffer("<html><head><title>") : null;
    if( sndMsg )
    { sb.append(resCode).append(' ').append(resMessage).append("</title></head><body>").append(resCode).append(resMessage).append("</body></html>").append("\r\n\r");
      setContentLength(sb.length()+1);
    }
    setContentType( "text/html" );
    if( sndMsg )
      out.println(sb.toString());
    // we close the connection for now
  	finalOut = true;
    out.close();
  }

    /// Sends a redirect message to the client using the specified redirect
    // location URL.
    // @param location the redirect location URL
    // @exception IOException if an I/O error has occurred
  public void sendRedirect( String location ) throws IOException
  { forceNoCache();
	setHeader( "Location", location );
  	sendError( SC_MOVED_TEMPORARILY );
  }

  private static String normalizeDomain( String domain, int port )
  {
    // normalize domain String
    //  - don't include leading "http://"
    //  - don't include trailing port from 'Prcs.cfg'
    //  - do include trailing port as specified by parameter -p (if non-default)
    if( domain == null )
      return null;
    if( domain.startsWith("http://") )
      domain = domain.substring(7);
    if( domain.startsWith("https://") )
      domain = domain.substring(8);
    int p = domain.indexOf(":/");
    if( (p=domain.indexOf(":",p+1)) > 0 )
      domain = domain.substring(0,p);
    if( (port!=80 && port!=443) || domain.indexOf(":/") >= 0 )
      domain = domain.concat(":").concat(new Integer(port).toString());
    return domain;
  }
    
  private String parseUri(String uri,int part)
  { return parseUri(uri, part, false);
  }
  
  private String parseUri(String uri,int part,boolean ssl_proxy)
  { if( uri == null )
      return null;
    int deli1 = uri.indexOf("://");
    int deli2 = uri.indexOf(':',deli1+1);
    int deli3 = uri.indexOf('/',deli1+3);
    switch( part )
    { case 1: // protocol
    		String p = uri.substring(0,deli1);
        return deli1<0 ? null : (ssl_proxy && p.equalsIgnoreCase("http") ? "https" : p);
      case 2: // host
        if( ++deli1 > 0 )
          deli1 +=2;
        else
          return null;  
        if( deli2 < 0 )
          deli2 = uri.indexOf('/',deli1);
        return deli2<0 ? uri.substring(deli1,deli3) : uri.substring(deli1,deli2);
      case 3: // port
        return deli1<0 || deli2<0 ? null : uri.substring(deli2+1);
      case 4: // path
        return deli1<0 ? uri : uri.substring(deli3);
    }
    return null;
  }

  private int parseInt( String s, int radix, int err_val )
  { try
    { int i = s.charAt(0)=='+' ? Integer.parseInt(s.substring(1), radix) : Integer.parseInt(s, radix);
      return i;
    }
    catch ( Exception ignore )
    {}
    return err_val;
  }

  Vector queryNames = null;
  Vector queryValues = null;

    /// Returns the parameter names for this request.
  public Enumeration getParameterNames()
  { if ( queryNames == null )
	{ queryNames = new Vector();
	  queryValues = new Vector();
	  String qs = "";
      String cType = ua_ndx < 0 ? null : cTypes[ua_ndx];
      if( reqMethod.equals( "POST" ) && cType!=null )
      { if( cType.startsWith("application/x-www-form-urlencoded") )
        { // set totalExpected here  - probably done already
          qs = getLine();
          // adjust totalExpected here - probably done already
        }
      }
      if( qs == null )
        qs = "";
      else if ( qs.length() > 0 )
	  { String[] pars = qs.split("&");
	  	for( int i=pars.length-1; i>=0; i-- )
	  	{ String par = pars[i];
	  	  String nv;
	  	  try
	  	  { nv = URLDecoder.decode(par,"UTF-8");
	  	  }
	  	  catch( Exception e )
	  	  { nv = "";
	  	  }
	      int eq = nv.indexOf( '=' );
   	  	  String name, value;
	      if ( eq == -1 )
		  { name = nv;
  		    value = "";
	  	  }
	      else
		  { name = nv.substring( 0, eq );
		    value = nv.substring( eq + 1 );
  		  }
	      queryNames.addElement( name );
	      queryValues.addElement( value );
	  	}
  	  }
    }
    return queryNames.elements();
  }

  /// Returns the value of the specified query string parameter, or null
  // if not found.
  // @param name the parameter name
  public String getParameter( String name )
  { getParameterNames();	// gets called because it ensures that 'queryNames' is initialized
  	int i = queryNames.indexOf( name );
	if ( i == -1 )
	  return null;
  	else
	  return (String) queryValues.elementAt( i );
  }
}

